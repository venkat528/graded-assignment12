create database Week12_haritha;
use Week12_haritha;

create table book(
       id integer not null auto_increment,
        title varchar(255),
        description varchar(255),
        published boolean ,
        primary key (id)
    ) ;
    
    create table user (
        id integer not null auto_increment,
        username varchar(20),
        email varchar(50),
        password varchar(120),
        primary key (id)
    ) ; 
create table roles (
        id integer not null auto_increment,
        name varchar(20),
        primary key (id)
    ) ; 