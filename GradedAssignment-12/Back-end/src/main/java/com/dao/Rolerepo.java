package com.dao;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.bean.ERole;
import com.bean.Role;

public interface Rolerepo extends JpaRepository<Role, Long> {

	Optional<Role> findByName(ERole name);
}
